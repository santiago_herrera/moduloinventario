package co.com.poli.ModuloInventario.path;

public class Path {

    public class PathProducto {

        public static final String PRODUCTO = "/producto";
        public static final String PRODUCTO_ID = "/producto/{idProducto}";
    }
}
