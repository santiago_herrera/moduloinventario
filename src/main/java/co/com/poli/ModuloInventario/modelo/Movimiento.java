
package co.com.poli.ModuloInventario.modelo;

import java.util.Objects;


public class Movimiento {
    private String id;
    private Producto producto;
    private TipoMovimiento tipoMoviento;
    private Double cantidad;
    private Double precio;

    public Movimiento(String id, Producto producto, TipoMovimiento tipoMoviento, Double cantidad, Double precio) {
        this.id = id;
        this.producto = producto;
        this.tipoMoviento = tipoMoviento;
        this.cantidad = cantidad;
        this.precio = precio;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public TipoMovimiento getTipoMoviento() {
        return tipoMoviento;
    }

    public void setTipoMoviento(TipoMovimiento tipoMoviento) {
        this.tipoMoviento = tipoMoviento;
    }

    public Double getCantidad() {
        return cantidad;
    }

    public void setCantidad(Double cantidad) {
        this.cantidad = cantidad;
    }

    public Double getPrecio() {
        return precio;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 59 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Movimiento other = (Movimiento) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }  
    
}


