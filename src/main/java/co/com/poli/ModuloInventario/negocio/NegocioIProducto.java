package co.com.poli.ModuloInventario.negocio;

import co.com.poli.ModuloInventario.modelo.Producto;
import java.util.List;

public interface NegocioIProducto {

    List<Producto> obtenerProductos();

    Producto obtenerProducto(String idProducto);

    String crearProducto(Producto producto);

    String eliminarProducto(String idProducto);

    String monidicarProducto(Producto producto);

}
