package co.com.poli.ModuloInventario.negocio.implementacion;

import co.com.poli.ModuloInventario.dao.IProductoDao;
import co.com.poli.ModuloInventario.datos.DatosPruducto;
import co.com.poli.ModuloInventario.modelo.Producto;
import co.com.poli.ModuloInventario.negocio.NegocioIProducto;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ImplementacionNegocioProducto implements NegocioIProducto {

    @Autowired
    private IProductoDao productoDao;

    @Override
    public List<Producto> obtenerProductos() {
        return productoDao.obtenerProductos();
    }

    @Override
    public Producto obtenerProducto(String idProducto) {
        return productoDao.obtenerProducto(idProducto);
    }

    @Override
    public String crearProducto(Producto producto) {

        if (producto.getExistencia() > 0) {
            Producto pro = productoDao.obtenerProducto(producto.getId());
            if (pro == null) {
                return productoDao.crearProducto(producto);
            } else {
                return "Ya existe el producto";
            }
        } else {
            return "La existencia debe ser mayor a cero";
        }

    }

    @Override
    public String eliminarProducto(String idProducto) {
        Producto pro = productoDao.obtenerProducto(idProducto);
        if (pro == null) {
            return "No existe el producto";
        } else {
            return productoDao.eliminarProducto(idProducto);

        }
    }

    @Override
    public String monidicarProducto(Producto producto) {
        if (producto.getExistencia() > 0) {
            Producto pro = productoDao.obtenerProducto(producto.getId());
            if (pro == null) {
                return "No existe el producto";
            } else {
                return productoDao.monidicarProducto(producto);
            }
        } else {
            return "La existencia debe ser mayor a cero";
        }
    }

}
