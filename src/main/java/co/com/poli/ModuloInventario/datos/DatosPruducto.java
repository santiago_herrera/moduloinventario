package co.com.poli.ModuloInventario.datos;

import co.com.poli.ModuloInventario.modelo.Producto;
import java.util.ArrayList;
import java.util.List;

public class DatosPruducto {

    private static List<Producto> listaProductos;

    static {
        listaProductos = new ArrayList<Producto>() {
            {
                add(new Producto("1010", "Leche", "Lacteos", 0D));
                add(new Producto("1020", "Queso", "Lacteos", 0D));
                add(new Producto("1030", "Quesito", "Lacteos", 0D));
            }
        };
    }

    public static List<Producto> getListaProductos() {
        return listaProductos;
    }

    public static void setListaProductos(List<Producto> listaProductos) {
        DatosPruducto.listaProductos = listaProductos;
    }

}
