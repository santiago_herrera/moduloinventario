package co.com.poli.ModuloInventario.controlador;

import co.com.poli.ModuloInventario.modelo.Producto;
import co.com.poli.ModuloInventario.negocio.NegocioIProducto;
import co.com.poli.ModuloInventario.path.Path.PathProducto;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import javax.websocket.server.PathParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/")
public class ProductoControlador {

    @Autowired
    private NegocioIProducto negocioIProducto;

    @GetMapping(PathProducto.PRODUCTO)
    public List<Producto> obtenerProductos(HttpServletResponse hsr) {
        return negocioIProducto.obtenerProductos();
    }

    @GetMapping(PathProducto.PRODUCTO_ID)
    public Producto obtenerProducto(@PathVariable String idProducto,
            HttpServletResponse hsr) {
        return negocioIProducto.obtenerProducto(idProducto);
    }

    @PostMapping(PathProducto.PRODUCTO)
    public String crearProducto(@RequestBody Producto producto,
            HttpServletResponse hsr) {
        return negocioIProducto.crearProducto(producto);
    }

    @DeleteMapping(PathProducto.PRODUCTO)
    public String eliminarProducto(@PathVariable String idProducto,
            HttpServletResponse hsr) {
        return negocioIProducto.eliminarProducto(idProducto);
    }

    @PutMapping(PathProducto.PRODUCTO)
    public String modificarProducto(@RequestBody Producto producto,
            HttpServletResponse hsr) {
        return negocioIProducto.monidicarProducto(producto);
    }

}
