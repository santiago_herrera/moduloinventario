package co.com.poli.ModuloInventario.dao;

import co.com.poli.ModuloInventario.modelo.Movimiento;
import java.util.List;

public interface IMovimientoDao { 

    List<Movimiento> obtenerMovimientos();

    Movimiento obtenerMovimiento(String idMovimiento);

    String crearMovimiento(Movimiento movimiento);

    String eliminarMovimiento(String idMovimiento);

    String modificarMovimiento(Movimiento movimiento);
}
