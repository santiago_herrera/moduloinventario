package co.com.poli.ModuloInventario.dao.implementacion;

import co.com.poli.ModuloInventario.dao.IProductoDao;
import co.com.poli.ModuloInventario.datos.DatosPruducto;
import co.com.poli.ModuloInventario.modelo.Producto;
import java.util.List;
import org.springframework.stereotype.Repository;

@Repository
public class ImplementacionDaoProducto implements IProductoDao {

    @Override
    public List<Producto> obtenerProductos() {
        return DatosPruducto.getListaProductos();
    }

    @Override
    public Producto obtenerProducto(String idProducto) {

        List<Producto> list = DatosPruducto.getListaProductos();
        Producto pro = null;

        for (Producto producto : list) {
            if (producto.getId().equals(idProducto)) {
                pro = producto;
            }

        }

        return pro;
    }

    @Override
    public String crearProducto(Producto producto) {
        List<Producto> list = DatosPruducto.getListaProductos();
        list.add(producto);
        DatosPruducto.setListaProductos(list);
        return "Producto creado!!!";
    }

    @Override
    public String eliminarProducto(String idProducto) {
        List<Producto> list = DatosPruducto.getListaProductos();
        Producto pro = obtenerProducto(idProducto);
        list.remove(pro);
        DatosPruducto.setListaProductos(list);
        return "Producto eliminado!!!";
    }

    @Override
    public String monidicarProducto(Producto producto) {
        List<Producto> list = DatosPruducto.getListaProductos();
        list.set(list.indexOf(producto), producto);
        DatosPruducto.setListaProductos(list);
        return "Producto modificado!!!";
    }

}
