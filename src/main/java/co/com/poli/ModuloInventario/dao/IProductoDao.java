package co.com.poli.ModuloInventario.dao;

import co.com.poli.ModuloInventario.modelo.Producto;
import java.util.List;

public interface IProductoDao { 

    List<Producto> obtenerProductos();

    Producto obtenerProducto(String idProducto);

    String crearProducto(Producto producto);

    String eliminarProducto(String idProducto);

    String monidicarProducto(Producto producto);
}
